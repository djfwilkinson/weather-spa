import Schema from 'validate'
import moment from 'moment'

export class WeatherDay {
  // overridden by data from the api call
  _tempMin = 1000 // degrees celcius

  // weather conditions - start values are outside the range of reasonable values to ensure they get
  _tempMax = -1000 // degrees celcius
  _windMax = 0 // mph
  _rainTotal = 0 // mm
  _pressureMin = 2000 // hPa
  _pressureMax = 0 // hPa
  _hasLightSnow = false

  /**
   * @param date Moment
   */
  constructor (date = moment()) {
    if (!moment.isMoment(date)) date = moment(date)
    this._date = date
  }

  /**
   * The schema used to validate the api response from OpenWeatherMap
   * @returns {Schema}
   */
  static get ExpectedApiSchema () {
    return new Schema({
      list: {
        type: Array,
        length: { min: 1 },
        each: {
          dt: {
            required: true,
            use: (val) => moment.unix(val).isValid()
          },
          main: {
            required: true,
            pressure: { required: true },
            temp: { required: true }
          },
          wind: {
            required: true,
            speed: { required: true }
          },
          clouds: {
            required: true,
            all: { required: true }
          },
          rain: {
            required: false,
            '3h': {
              required: false
            }
          },
          snow: {
            required: false,
            '3h': {
              required: false
            }
          },
          weather: {
            type: Array,
            length: { min: 1 },
            each: {
              id: { required: true }
            }
          }
        }
      }
    })
  }

  // moment object at 'start of day'
  _date = moment()

  // public getters
  get date () {
    return this._date
  }

  // These booleans will be used to determine which icon to use later on
  _hasLightCloud = false

  get hasLightCloud () {
    return this._hasLightCloud
  }

  _hasHeavyCloud = false

  get hasHeavyCloud () {
    return this._hasHeavyCloud
  }

  _hasLightRain = false

  get hasLightRain () {
    return this._hasLightRain
  }

  _hasHeavyRain = false

  get hasHeavyRain () {
    return this._hasHeavyRain
  }

  _hasHeavySnow = false

  get hasHeavySnow () {
    return this._hasHeavySnow
  }

  _hasThunderstorm = false

  get hasThunderstorm () {
    return this._hasThunderstorm
  }

  get temp () {
    return { min: this._tempMin, max: this._tempMax }
  }

  get wind () {
    return this._windMax
  }

  get rain () {
    return this._rainTotal
  }

  get pressure () {
    return { min: this._pressureMin, max: this._pressureMax }
  }

  /**
   * Parses an OpenWeatherMap api response into an array of 5 WeatherDay objects.
   * The first data item will be used to determine the first date of the first object.
   * A new WeatherDay instance will be created for each date encountered.
   * We're trusting the API response has ordered the dates correctly.
   * @param apiResponse String - the response previously retrieved from OpenWeatherMap
   * @returns WeatherMap[5]
   */
  static ParseOpenWeatherMap (apiResponse) {
    // if the response is invalid (signified by a non empty error array) then return an empty array
    if (WeatherDay.ExpectedApiSchema.validate(apiResponse).length) {
      console.error('Found errors in returned OpenWeatherMap data', WeatherDay.ExpectedApiSchema.validate(apiResponse))
      return []
    }
    let response = []

    // we are sure there's at least one item in the array because we validated the data
    let currentDate = moment.unix(apiResponse.list[0].dt).startOf('day')
    let wd = new WeatherDay(currentDate)

    // loop through OpenWeatherMap items and update the WeatherDay instance as we go
    for (const owmObj of apiResponse.list) {
      if (!currentDate.isSame(moment.unix(owmObj.dt), 'day')) {
        // this is the next day - start a new WeatherDay instance
        response.push(wd) // add the previous one to the array
        currentDate = moment.unix(owmObj.dt).startOf('day')
        wd = new WeatherDay(currentDate)
      }

      let windSpeedMph = owmObj.wind.speed * 2.237 // convert from m/s to mph

      // min/maxs
      wd._tempMin = owmObj.main.temp < wd._tempMin ? owmObj.main.temp : wd._tempMin
      wd._tempMax = owmObj.main.temp > wd._tempMax ? owmObj.main.temp : wd._tempMax
      wd._windMax = windSpeedMph > wd._windMax ? windSpeedMph : wd._windMax
      wd._pressureMin = owmObj.main.pressure < wd._pressureMin ? owmObj.main.pressure : wd._pressureMin
      wd._pressureMax = owmObj.main.pressure > wd._pressureMax ? owmObj.main.pressure : wd._pressureMax

      // accumulations
      if (owmObj.rain && owmObj.rain['3h']) wd._rainTotal += parseFloat(owmObj.rain['3h'])

      // booleans
      // rain intensity values taken roughly from wikipedia - https://en.wikipedia.org/wiki/Rain#Intensity
      wd._hasLightRain = wd._hasLightRain || (owmObj.rain && owmObj.rain['3h'] && owmObj.rain['3h'] > 0)
      wd._hasHeavyRain = wd._hasHeavyRain || (owmObj.rain && owmObj.rain['3h'] && owmObj.rain['3h'] > 15) // our time periods are 3h
      // cloudy levels are made up from personal choice (the values are percentage cloud cover)
      wd._hasLightCloud = wd._hasLightCloud || owmObj.clouds.all > 10 // over 10% cloud cover indicates light cloud (less would be clear sky)
      wd._hasHeavyCloud = wd._hasHeavyCloud || owmObj.clouds.all > 75 // over 75% cloud cover indicates heavy cloud (eg overcast)
      // snow intensity values use same as rain above
      wd._hasLightSnow = wd._hasLightSnow || (owmObj.snow && owmObj.snow['3h'] && owmObj.snow['3h'] > 0)
      wd._hasHeavySnow = wd._hasHeavySnow || (owmObj.snow && owmObj.snow['3h'] && owmObj.snow['3h'] > 15)
      // run through the weather id's to determine if any are thunderstorm (https://openweathermap.org/weather-conditions)
      for (const owmWeather of owmObj.weather) {
        wd._hasThunderstorm = wd._hasThunderstorm || (owmWeather.id >= 200 && owmWeather.id < 300)
      }
    }

    // note at this point we could add the final WeatherDay object to the array.
    // however, typically this object is the 6th day and doesn't contain full data for the entire day.
    // so if we just don't add it then that prevents our output being misleading
    // (and we still have 5 days weather)

    return response
  }
}

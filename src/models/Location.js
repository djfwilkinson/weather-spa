export class Location {
  constructor (name, lat, lng) {
    this._name = name
    // store lat and lng as strings
    this._lat = `${lat}`
    this._lng = `${lng}`
  }

  // array of location types we will treat as valid as returned by LocationIQ
  static get ValidTypes () {
    return ['city', 'suburb', 'town', 'village', 'hamlet']
  }

  _name = ''

  get name () {
    return this._name
  }

  _lat = ''

  get lat () {
    return this._lat
  }

  _lng = ''

  get lng () {
    return this._lng
  }

  /**
   * Converts a LocationIQ result data item into a Location
   * @param iqModel
   * @returns Location|null
   */
  static CreateFromLocationIQ (iqModel) {
    // some checks on the data
    if (!Location.IsValidLocationIQ(iqModel)) return null

    // work out the address string we want
    let address = iqModel.address.city
    if (iqModel.address.county) address = `${address}, ${iqModel.address.county}`
    if (iqModel.address.suburb) address = `${iqModel.address.suburb}, ${address}`

    return new Location(address, iqModel.lat, iqModel.lon)
  }

  /**
   * Helper method used to filter the array of items returned by LocationIQ.
   * We are only interested in a subset of the results (for usability reasons).
   * @param iqModel Location IQ JSON Api response item
   * @returns {boolean} Is this item a valid Location for us
   */
  static IsValidLocationIQ (iqModel) {
    // no type? this is bad data
    if (!iqModel.type) return false
    // we only want cities, suburbs and towns
    if (!Location.ValidTypes.includes(iqModel.type)) return false
    // most addresses should have a city data in them and it creates better results for us
    if (!iqModel.address || !iqModel.address.city) return false
    // need lat and lon values
    if (!iqModel.lat || !iqModel.lon) return false
    // else we're good
    return true
  }
}

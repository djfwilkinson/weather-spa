import { Selector } from 'testcafe'

// A fixture must be created for each group of tests.
fixture(`Weather SPA`)
// Load the URL your development server runs on.
  .page('http://localhost:8080')

test('starts up with no location selected shown', async testController => {
  // title bar
  const titleBarText = await new Selector('div[title="Click here to change Location"]')
  // details card
  const detailsCardText = await new Selector('div.headline')

  // Assert
  await testController.expect(titleBarText.innerText).eql('No Location Selected')
  await testController.expect(detailsCardText.innerText).eql('No location selected')
})

test('loads and displays weather for a location once a user has searched for and selected it', async testController => {
  // click search
  const searchBtn = await new Selector('button[title="Change Location"]')
  await testController.click(searchBtn)

  // enter text
  const searchInput = await new Selector('input[type="text"]')
  await testController.typeText(searchInput, 'London', { replace: true })

  // select result
  const firstSearchResult = await new Selector('a').withText('London')
  await testController.click(firstSearchResult)

  // should use this text as the header text
  const titleBarText = await new Selector('div[title="Click here to change Location"]')
  await testController.expect(titleBarText.innerText).eql('London')

  // we should have five tabs with content
  const tabs = await new Selector('div.v-tabs__div')
  const tabContent = await new Selector('div.v-tabs__content')
  await testController.expect(tabs.count).eql(5)
  await testController.expect(tabContent.count).eql(5)

  // we should have weather data
  const tempMin = await new Selector('div.min-temp')
  const tempMax = await new Selector('div.max-temp')
  const rain = await new Selector('div.rain')
  const wind = await new Selector('div.wind')
  const pressure = await new Selector('div.pressure')
  await testController.expect(tempMin.innerText).match(/(-?[0-9]{1,2}℃)/)
  await testController.expect(tempMax.innerText).match(/(-?[0-9]{1,2}℃)/)
  await testController.expect(rain.innerText).match(/(([0-9]+.)[0-9]+ mm)/)
  await testController.expect(wind.innerText).match(/([0-9]+ mph)/)
  await testController.expect(pressure.innerText).match(/([0-9]+ - [0-9]+ hPa)/)
})

import chai from 'chai'
import { shallowMount } from '@vue/test-utils'
import Vuetify from 'vuetify'
import DetailsCard from '../../../src/components/DetailsCard.vue'
import Vue from 'vue'
import { Location } from '../../../src/models/Location'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'
import axios from 'axios'
import { WeatherDay } from '../../../src/models/WeatherDay'

chai.use(sinonChai)
const expect = chai.expect

describe('components/DetailsCard', () => {
  let axiosResponse = null

  beforeEach(() => {
    Vuetify.install(Vue)
    sinon.stub(axios, 'get').callsFake(() => {
      return new Promise(() => axiosResponse)
    })
  })

  afterEach(() => {
    sinon.restore()
  })

  it('renders a vue instance', () => {
    let wrp = shallowMount(DetailsCard)
    expect(wrp.isVueInstance()).to.be.true
  })

  it('displays no location if one isn\'t set', () => {
    let wrp = shallowMount(DetailsCard)

    wrp.setProps({
      location: null
    })

    expect(wrp.find({ ref: 'details-card-no-location-v-card' }).exists()).to.be.true
    expect(wrp.find({ ref: 'details-card-tabs' }).exists()).to.be.false
    expect(wrp.find({ ref: 'details-card-loading-v-card' }).exists()).to.be.false
    expect(wrp.find({ ref: 'details-card-error-v-card' }).exists()).to.be.false
  })

  it('loads weather data if location is set', () => {
    let wrp = shallowMount(DetailsCard)

    wrp.setProps({
      location: new Location('test', 1, 2)
    })

    expect(axios.get).to.have.been.calledOnce
  })

  it('displays loading if location is set and loading is true', () => {
    const wrp = shallowMount(DetailsCard, {
      methods: {
        fetchOpenWeatherMap: sinon.stub().returns(new Promise(() => null))
      }
    })

    wrp.setProps({
      location: new Location('test', 1, 2)
    })

    expect(wrp.find({ ref: 'details-card-no-location-v-card' }).exists()).to.be.false
    expect(wrp.find({ ref: 'details-card-tabs' }).exists()).to.be.false
    expect(wrp.find({ ref: 'details-card-loading-v-card' }).exists()).to.be.true
    expect(wrp.find({ ref: 'details-card-error-v-card' }).exists()).to.be.false
  })

  it('displays error message if location is set, loading is false, and there\'s no data', (done) => {
    const wrp = shallowMount(DetailsCard, {
      methods: {
        fetchOpenWeatherMap: () => Promise.reject(new Error('fake fetch fail'))
      }
    })

    wrp.setProps({
      location: new Location('test', 1, 2)
    })

    setTimeout(() => {
      expect(wrp.find({ ref: 'details-card-no-location-v-card' }).exists()).to.be.false
      expect(wrp.find({ ref: 'details-card-tabs' }).exists()).to.be.false
      expect(wrp.find({ ref: 'details-card-loading-v-card' }).exists()).to.be.false
      expect(wrp.find({ ref: 'details-card-error-v-card' }).exists()).to.be.true
      done()
    })
  })

  it('creates a tab for each WeatherDay', (done) => {
    const weatherArr = [
      new WeatherDay(),
      new WeatherDay(),
      new WeatherDay()
    ]
    const wrp = shallowMount(DetailsCard, {
      propsData: {
        location: new Location('test', 1, 2)
      }
    })

    wrp.setData({
      weatherArr: weatherArr
    })

    setTimeout(() => {
      expect(wrp.find({ ref: 'details-card-no-location-v-card' }).exists()).to.be.false
      expect(wrp.find({ ref: 'details-card-tabs' }).exists()).to.be.true
      expect(wrp.find({ ref: 'details-card-loading-v-card' }).exists()).to.be.false
      expect(wrp.find({ ref: 'details-card-error-v-card' }).exists()).to.be.false
      expect(wrp.findAll({ ref: 'details-card-tab' }).length).to.equal(3)
      done()
    })
  })
})

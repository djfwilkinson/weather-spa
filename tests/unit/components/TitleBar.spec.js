import chai from 'chai'
import { shallowMount, mount } from '@vue/test-utils'
import Vuetify from 'vuetify'
import TitleBar from '../../../src/components/TitleBar.vue'
import Vue from 'vue'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'
import axios from 'axios'

chai.use(sinonChai)
const expect = chai.expect

describe('components/TitleBar', () => {
  let axiosResponse = null

  beforeEach(() => {
    Vuetify.install(Vue)
    sinon.stub(axios, 'get').callsFake(() => {
      return new Promise(() => axiosResponse)
    })
  })

  afterEach(() => {
    sinon.restore()
  })

  /**
   * This is a workaround as described on a Vuetify github issue
   * See https://github.com/vuetifyjs/vuetify/issues/3456
   * @param options
   * @returns {Wrapper<Vue>}
   */
  function getFullMount (options = {}) {
    const el = document.createElement('div')
    el.setAttribute('data-app', true)
    document.body.appendChild(el)

    window.requestAnimationFrame = callback => {
      setTimeout(callback, 0)
    }

    return mount(TitleBar, options)
  }

  it('renders a vue instance', () => {
    let wrp = shallowMount(TitleBar)
    expect(wrp.isVueInstance()).to.be.true
  })

  it('displays no location if one isn\'t set', () => {
    let wrp = getFullMount()

    wrp.setData({
      result: null,
      searchMode: false
    })

    expect(wrp.find({ ref: 'title-bar-title-text' }).exists()).to.be.true
    expect(wrp.find({ ref: 'title-bar-search' }).exists()).to.be.false
    expect(wrp.vm.currentSearchIcon).to.equal('search')
    expect(wrp.find({ ref: 'title-bar-title-text' }).text()).to.equal('No Location Selected')
  })

  describe('geolocate button', () => {
    it('is hidden when not supported', () => {
      let wrp = getFullMount({
        methods: {
          canGeolocate: sinon.stub().returns(false)
        }
      })

      expect(wrp.find({ ref: 'title-bar-geolocate-btn' }).exists()).to.be.false
    })

    it('is shown when supported', () => {
      let wrp = getFullMount({
        methods: {
          canGeolocate: sinon.stub().returns(true)
        }
      })

      expect(wrp.find({ ref: 'title-bar-geolocate-btn' }).exists()).to.be.true
    })
  })

  describe('.setSearchMode()', () => {
    it('updates the title bar icon and focuses the search field', () => {
      const searchStub = sinon.stub()
      let wrp = getFullMount({
        methods: {
          focusSearchInput: searchStub
        }
      })

      wrp.vm.setSearchMode(true)
      expect(wrp.find({ ref: 'title-bar-search-icon' }).text()).to.equal('close')

      wrp.vm.setSearchMode(false)
      expect(wrp.find({ ref: 'title-bar-search-icon' }).text()).to.equal('search')

      expect(searchStub).to.have.been.calledOnce
    })
  })
})

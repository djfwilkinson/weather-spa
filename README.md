# Weather SPA
An example VueJS single page application which allows the user to search for a location, select it and see five days worth of weather for that location.

## This README.md file
This README.md contains project set up information only. For my approach to the task please see the readme file within the root of the supplied archive. 

## Project setup
1. Install node [https://nodejs.org/en/]() which includes `npm`
2. In the `project` folder run:
    ```
    npm install
    ```

## Project commands
### Compiles and hot-reloads for development
```
npm run serve
```
Serves the application on a local server at [http://localhost:8080]()

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Run end to end tests
```
npm run test:e2e
```
*Note: e2e tests require browsers to be installed on the test system. [TestCafe](https://devexpress.github.io/testcafe/) should pick up installed browsers automatically.*
